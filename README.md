# next-boilerplate

Simple boilerplate to start a Next.js project.

## What's include?

This boilerplate uses:

- [Next.js](https://nextjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Styled Components](https://styled-components.com/)
- [ESLint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [Jest](https://jestjs.io/)
- [React Testing Library (RTL)](https://testing-library.com/docs/react-testing-library/intro)
- [Husky](https://github.com/typicode/husky)

## Usage

Here's an example to run the development server:

```bash
npm run dev
# or
yarn dev
```
