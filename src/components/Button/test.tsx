import { screen, render } from '@testing-library/react';

import Button from '.';

describe('<Button />', () => {
  it('should render the button element', () => {
    render(<Button value="button component" />);

    expect(
      screen.getByRole('button', { name: /button component/i }),
    ).toBeInTheDocument();
  });
});
