type Props = {
  value: string;
};

const Button = ({ value }: Props) => <button>{value}</button>;

export default Button;
