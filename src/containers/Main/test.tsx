import { screen, render } from '@testing-library/react';

import Main from '.';

describe('Main container', () => {
  it('should render the container', () => {
    const { container } = render(<Main />);

    expect(screen.getByRole('main')).toBeInTheDocument();

    expect(container.firstChild).toMatchSnapshot();
  });
});
