import styled from 'styled-components';

import { colors, typography } from '../../styles/theme';

export const Container = styled.main`
  width: 100%;
  height: 100%;

  color: ${colors.muted.m6};
  font-size: ${typography.body.p1.fontSize};
`;
