import * as S from './styles';

const Main = () => (
  <S.Container>
    <p>Main container!</p>
  </S.Container>
);

export default Main;
