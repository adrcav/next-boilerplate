import { createGlobalStyle } from 'styled-components';

import { colors } from './theme';

const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  html,
  body {
    margin: 0;
    font-family: 'Lato', sans-serif;
    font-weight: 400;
    font-size: 62.5%;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    background-color: ${colors.base.light};
  }

  a {
    color: inherit;
    text-decoration: none;
  }
`;

export default GlobalStyles;
